import numpy as np
import matplotlib.pyplot as plt

def makePlot(xVals, yVals, xErr=None, yErr=None , function_x=None, R2=None, s2=None, A=None, dA=None, B= None, dB= None, lstyl=None, titel=None, xbeschr=None, ybeschr=None, function=None):
    
    fig = plt.figure()
    #ax = fig.add_subplot(1,1,1)

    plt.errorbar(xVals,yVals,yerr=yErr,fmt=".",color="black",capsize=6,capthick=2,label="Messpunkte",linestyle=lstyl)

    if function!=None:
        xkoords=np.linspace(function_x[0],function_x[-1]+10,10)               #x-Wertliste für das Diagramm
        plt.plot(xkoords, function(xkoords), color='blue')
    if A != None:
        plt.plot(xkoords,A+B*xkoords,color="blue",label="Ausgleichsgerade")     #Ausgleichsgerade
        plt.plot(xkoords,A+dA+(B+dB)*xkoords,color="red")                       #obere Grenzgerade
        plt.plot(xkoords,A-dA+(B-dB)*xkoords,color="red",label="Grenzgeraden")  #untere Grenzgerade
    if R2 != None:
        plt.figtext(.5,.3, s="Steigung C = {}\u00B1{}".format(B,dB))
        plt.figtext(.4,.2, s="Bestimmtheitsmaß = {}\nVarianz = {}".format(R2,s2))
    #plt.legend(loc="best") 
    #                                                  #Legende erstellen
    # Major ticks every 20, minor ticks every 5
    """major_ticksx = np.arange(400, 600, 10)
    minor_ticksx = np.arange(400, 600, 1)
    major_ticksy = np.arange(1.65, 1.75, 0.01)
    minor_ticksy = np.arange(1.65, 1.75, 0.001)

    ax.set_xticks(major_ticksx)
    ax.set_xticks(minor_ticksx, minor=True)
    ax.set_yticks(major_ticksy)
    ax.set_yticks(minor_ticksy, minor=True)

    ax.grid(which="major",alpha=0.6)
    ax.grid(which="minor",alpha=0.3)"""
    plt.grid()
    plt.title(titel)                                               #Titel erstellen
    plt.xlabel(xbeschr)                                                 #Beschriftung der x-Achse
    plt.ylabel(ybeschr)                                                 #Beschriftung der y-Achse
    plt.show()