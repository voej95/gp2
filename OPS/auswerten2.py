import numpy as np
import matplotlib.pyplot as plt
from Rundung import *

def support(xwerte, ywerte, yfehler):              #Funktionsdefinition
    """Berechnet die Summen über x, x^2, y, xy und 1, jeweils durch den Fehler^2."""

    #Summen (38a) und (38b)
    x2=0                                         #Summe über (x^2)/(sigma^2)
    for i in range(len(xwerte)):                 
        x2 += (xwerte[i]**2) / (yfehler[i]**2)   
    x=0                                          #Summe über (x)/(sigma^2)
    for i in range(len(xwerte)):
        x += xwerte[i] / (yfehler[i]**2)
    y=0                                          #Summe über (y)/(sigma^2)
    for i in range(len(ywerte)):
        y += ywerte[i] / (yfehler[i]**2)
    xy=0                                         #Summe über (x*y)/(sigma^2)
    for i in range(len(xwerte)):
        xy += (xwerte[i]*ywerte[i]) / (yfehler[i]**2)
    eins=0                                       #Summe über (1)/(sigma^2)
    for i in range(len(xwerte)):
        eins += 1 / (yfehler[i]**2)

    return rundung(x), rundung(x2), rundung(y), rundung(xy), rundung(eins) #Ausgabe der Werte mit Rundung

def bestimmt(a,b,xwerte, ywerte, yfehler):

    sup = support(xwerte, ywerte, yfehler)
    yquer = sup[2]/sup[4]                                     #Berechnung von yquer

    zähler=0                                                  #Zähler von R
    for i in range(len(ywerte)):                              #Summe über die Abweichungen von der Ausgleichsgeraden
        zähler += ((ywerte[i]-a-b*xwerte[i])/(yfehler[i]))**2 
    nenner=0                                                  #Nenner von R
    for i in range(len(ywerte)):                              #Summe über die Abweichungen vom Mittelwert yquer
        nenner += ((ywerte[i]-yquer)/(yfehler[i]))**2  
        
    s2 = zähler/(len(xwerte)-1)

    return rundung((1-zähler/nenner)*100,zwischen=False), rundung(s2,zwischen=False)

def kern(xwerte, ywerte, yfehler):
    """Berechnet a, b, Delta a und Delta b."""

    (x,x2,y,xy,eins)=support(xwerte, ywerte, yfehler)  #erhält Hilfssummen aus support
    S = eins*x2-x**2                                        #Determinante der Koeffizientenmatrix S
    a=(x2*y-x*xy)/S                                         #y-Achsenabschnitt
    b=(eins*xy-x*y)/S                                       #Steigung 
    da=np.sqrt(x2/S)                                        #Fehler des y-Achsenabschnitts
    db=np.sqrt(eins/S)                                      #Fehler der Steigung 

    return rundung(a,da,zwischen=False), rundung(b,db,zwischen=False)

def main(messung):
    xwerte = messung.freq
    ywerte = messung.yVals
    yfehler = messung.yErr
        
    #ywerte = np.log(ywerte)
    #yfehler = yfehler/ywerte
    (A,dA),(B,dB)=kern(xwerte, ywerte, yfehler)
    R2,s2 = bestimmt(A,B,xwerte, ywerte, yfehler)

    return (A,dA,B,dB,R2,s2) 



