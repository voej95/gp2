import numpy as np

class Messung():

    def __init__(self, filename, xachse="x", yachse="y", titel=None) -> None:
        #Beschriftungen
        self.xachse = xachse
        self.yachse = yachse
        self.titel = titel
        #Messwerte
        self.gem_winkel, self.farbe, self.intens = self.getData(filename)
        self.axe_data = [self.gem_winkel, self.farbe, self.intens]
        
    def getData(self,filename):
        gem_winkel,farbe,intensitaet = np.genfromtxt(filename,delimiter=';',dtype='str').T
        return gem_winkel.astype(np.float64),farbe,intensitaet.astype(np.float64)

    def geraetfehlerBerechnen(self, messwert, genauigkeit, digits):
        return messwert/100*genauigkeit+0.0001*digits
    
def fehlerfortpflanzung(v, a, da, b=None, db=None):
    rel_a = da/a
    if b != None:
        rel_b = db/b
    else: rel_b = 0
    dv = np.sqrt(rel_a**2+rel_b**2)*v
    return dv
    
