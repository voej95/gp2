import numpy as np
from Messung import Messung
from pathlib import Path
from makePlot import makePlot

"""#Aufgabe 1
b = 33 #mm
db = 0.5 #mm
f = 120 #mm

#Aufgabe 2
gam_1 = 115 #°
gam_2 = 239.4 #°
dgam = 0,2 #°

wink_brech = gam_2 - gam_1

#Aufgabe 3
d_gem_winkel = 0.005 #°
d_lamdba = 0.05 #nm

winkelmessungdata = Path("OPS/winkelmessung.txt")

mess_tab = Messung(winkelmessungdata)

rel_winkel = 180 - mess_tab.gem_winkel #°

#Zuordnung der Farben zur Wellenlänge
#404,66 nm (sichtbares Licht violett),435,83 nm (sichtbares Licht blau),491,60 nm (sichtbares Licht cyan)
#546,07 nm (sichtbares Licht grün),576,96 nm (sichtbares Licht orange),579,07 nm (sichtbares Licht orange)
#614,95 nm (sichtbares Licht rot)
farb_nanometers = [('violett', 404.66), ('blau', 435.83), ('cyan', 491.60), ('gruen', 546.07), ('gelb', 576.96), ('dunkelgelb', 579.07)]

#Korrektur der Farben anhand von bekannten Hg Linien
#dunkelblau -> blau , dunkelgruen -> cyan
pos = np.where(mess_tab.farbe == 'dunkelblau')
mess_tab.farbe[pos] = 'blau'
pos = np.where(mess_tab.farbe == 'dunkelgruen')
mess_tab.farbe[pos] = 'cyan'

#generiere liste mit passenden Wellenlängen
lambdas = []
for i in range(len(mess_tab.farbe)):
    if mess_tab.farbe[i] == farb_nanometers[i][0]:
        lambdas.append(farb_nanometers[i][1])

#erstelle Listen mit Fehlern
d_lamdas = []
d_rel_winkel = []
for i in range(len(lambdas)):
    d_lamdas.append(d_lamdba)
    d_rel_winkel.append(d_gem_winkel)

makePlot(rel_winkel,lambdas,xErr=d_rel_winkel,yErr=d_lamdas,titel="Quecksilber Kallibrierkurve",xbeschr="Ablenkwinkel $\gamma$ [°]",ybeschr="Wellenänge $\lambda$ [nm]",lstyl='-')"""



lambdas_disp = [404.7,435.8,491.6,546.1,579.1]
n = [1.6686, 1.68216, 1.71462, 1.73752, 1.73872]
dn = [0.00609188, 0.00611032, 0.00617637, 0.00625445, 0.00625289]

makePlot(lambdas_disp, n, yErr=dn, titel="Dispersionskurve von Quecksilber", xbeschr="Wellenänge $\lambda$ [nm]", ybeschr="Brechungsindex n")